﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Devoir1
{
    class Program
    {
        static void Main(string[] args)
        {            
            List<Employe> tab = new List<Employe>();
            string matricule = "";


            do
            {
                Console.Clear();
                Console.Write("\t\t\t\tBIENVENUE\n");
                Console.WriteLine("\t\t\t\t---------");
                Console.WriteLine("\t\t\t\t---------");

                Console.Write("\nEntrez le matricule : ");
                matricule = Console.ReadLine();

                if (matricule.Length == 0)
                    break;

                Console.Write("Entrez le nom complet : ");
                string nom = Console.ReadLine();

                Console.Write("Entrez le poste : ");
                string post = Console.ReadLine();

                int salaire = Controle.saisieEntier("Entrez le salaire : ");

                DateTime dat = Controle.saisieDate("Entrez la date d'embauche : ");

                Console.WriteLine("Choisissez le service : ");
                Console.WriteLine("1. Informatique");
                Console.WriteLine("2. Marketing");
                Console.WriteLine("3. Comptabilite");
                int choice = 0;
                do
                {
                    choice = Controle.ConsoleKeyInt("Votre choix : ");
                
                } while (choice > 3 && choice < 1);

                Service serv = null;

                switch(choice)
                {
                    case 1 :

                        serv = new Service("Informatique", 1);

                        break;

                    case 2:

                        serv = new Service("Marketing", 2);

                        break;

                    case 3:

                        serv = new Service("Comptabilite", 3);

                        break;
                }
                Employe p = new Employe(matricule, nom, post, salaire, dat, serv);
                tab.Add(p);
            
            }while(matricule.Length != 0);

            Console.Clear();

            Console.WriteLine("\nEMPLOYE QUI CELEBRENT LEUR ANNIVERSAIRE");
            Console.WriteLine("---------------------------------------\n");

            byte i = 0;
            int sal = 0, parcours = 0;
            Employe max = null;

            if (tab.Count != 0)
            {
                foreach (Employe e in tab)
                {

                    if ((e.m_dateEmb.Day == DateTime.Now.Day) && (e.m_dateEmb.Month == DateTime.Now.Month))
                    {
                        e.Afficher();
                        Console.WriteLine("");
                        ++i;
                    }

                    if (e.m_salaire > sal)
                    {
                        max = e;
                        sal = e.m_salaire;
                    }

                    ++parcours;
                }

                if (i == 0)
                    Console.WriteLine("Aucun employe ne celebre son anniversaire aujourd'hui\n");

                Console.WriteLine("\t\t--------------------");
                Console.WriteLine("\t\t--------------------");


                Console.WriteLine("\nEMPLOYE AYANT LE PLUS GROS SALAIRE");
                Console.WriteLine("------------------------------------\n");

                if (max != null)
                {
                    Console.WriteLine("Employe : " + max.m_nomCplt + 
                        "\nMatricule : " + max.m_matricule + "\nService : " + max.getService().m_nom + "\n");
                }
                else Console.WriteLine("Tous les employes ont le meme salaire ou la liste est vide.");

            }
            else Console.WriteLine("Aucun element dans la liste...\n");

        }
    }
}
