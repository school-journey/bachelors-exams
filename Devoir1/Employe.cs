﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Devoir1
{
    class Employe
    {
        public string m_matricule { get; set;}
        public string m_nomCplt { get; set; }
        public string m_poste;
        public int m_salaire { get; set;  }
        public DateTime m_dateEmb;
        private Service m_service = new Service();

        public Employe()
        {
            m_matricule = m_nomCplt = m_poste = "Inconnu";
            m_salaire = 0;
            m_dateEmb = DateTime.Now;
        }

        public Service getService()
        {
            return m_service;
        }

        public Employe(string matr, string nom, string post, int sal, DateTime dat, Service s)
        {
            m_service = s;

            if (nom.Length != 0)
                m_nomCplt = nom;

            if (post.Length != 0)
                m_poste = post;

            if (matr.Length != 0)
                m_matricule = matr;

            if (sal != 0)
                m_salaire = sal;

            m_dateEmb = dat;

        }

        public void Afficher()
        {
            Console.WriteLine("Matricule : " + m_matricule + "\nNom Complet : " + m_nomCplt + "\nPoste : " + m_poste 
                + "\nSalaire : " + m_salaire + "\nDate d'embauche : " + getNaissanceLitteral() + "\nService : " + m_service.Show());
        }

        private string getNaissanceLitteral()
        {
            string date = "";

            foreach (var f in Mois.GetValues(typeof(Mois)))
            {
                if ((int)f == m_dateEmb.Month - 1)        //Si le num de l'enum Mois [(int)f] est = au num du mois - 1 (decalage car l'enum commence a partir de 0) 
                    date = m_dateEmb.Day + " " + f + " " + m_dateEmb.Year;
            }

            return date;
        }

    }


    /**********************/
    enum Mois
    {
        Janvier, Fevrier, Mars, Avril, Mai, Juin, Juillet, Aout, Septembre, Octobre, Novembre, Decembre
    }

}
