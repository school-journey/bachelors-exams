﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Devoir1
{
    class Service
    {
        public string m_nom { get; set; }
        public int m_nums { get; set; }

        public Service()
        {
            m_nom = "Inconnu";
            m_nums = 0;
        }

        public Service(string nom, int num)
        {
            if (nom.Length != 0)
                m_nom = nom;

            if (num != 0)
                m_nums = num;

        }

        public string Show()
        {
            return (m_nom + " ( " + m_nums + " )");
        }
    }
}
