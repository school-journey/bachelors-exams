﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Devoir1
{
    class Controle
    {
       
        ///<summary>
        /// Methode de controle de saisie de type Datetime
        ///</summary>

        public static DateTime saisieDate(string txt)
        {
            bool valid = true;

            do
            {
                Console.Write(txt);

                try
                {
                    valid = true;
                    return DateTime.Parse(Console.ReadLine());

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Erreur : " + ex.Message);
                    valid = false;
                }

            } while (!valid);

            return DateTime.Parse("0");
        }

        /*********************************************************/

        ///<summary>
        /// Controle de saisie d'Entier
        ///</summary>
        public static int saisieEntier(string message)
        {

            int val = 0;
            bool valid = true;

            do
            {
                try
                {
                    valid = true;
                    Console.Write(message);
                    val = int.Parse(Console.ReadLine());
                }
                catch (Exception ex)
                {
                    valid = false;
                    Console.Error.WriteLine("Nombre invalide. " + ex.Message);
                }

            } while (!valid);

            return val;
        }       

        /// <summary>
        /// Controle de saisie d'un entier sans validation par Entree
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static int ConsoleKeyInt(string message)
        {

            ConsoleKeyInfo val;
            bool valid = true;

            do
            {
                Console.Write(message);
                val = Console.ReadKey(true);
                int a;

                if (int.TryParse(val.KeyChar.ToString(), out a))
                {
                    valid = true;
                    return a;
                }
                else
                {
                    valid = false;
                    Console.Error.WriteLine("Nombre invalide.");
                }

            } while (!valid);

            return 0;
        }

        /********************   FINNNNNNNNNN    *************************************/

    }
}
