﻿using Devoir2.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Devoir2
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private LinqDataContext db = Connexion.dataz;
        Devoir2.GestionProjetDataSetTableAdapters.EmployeTableAdapter gestionProjetDataSetEmployeTableAdapter;
        Devoir2.GestionProjetDataSet gestionProjetDataSet;
        Devoir2.GestionProjetDataSetTableAdapters.ProjetTableAdapter gestionProjetDataSetProjetTableAdapter;
        Devoir2.GestionProjetDataSetTableAdapters.AffectationTableAdapter gestionProjetDataSetAffectationTableAdapter;
        Devoir2.GestionProjetDataSetTableAdapters.projet_en_coursTableAdapter gestionProjetDataSetprojet_en_coursTableAdapter;
        System.Windows.Data.CollectionViewSource employeViewSource;

        public MainWindow()
        {
            InitializeComponent();
            cbxProjet.ItemsSource = from e in db.Projet select e;
            
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            gestionProjetDataSet = ((Devoir2.GestionProjetDataSet)(this.FindResource("gestionProjetDataSet")));
            // Chargez les données dans la table Employe. Vous pouvez modifier ce code si nécessaire.
            gestionProjetDataSetEmployeTableAdapter = new Devoir2.GestionProjetDataSetTableAdapters.EmployeTableAdapter();
            gestionProjetDataSetEmployeTableAdapter.Fill(gestionProjetDataSet.Employe);
            employeViewSource = ((System.Windows.Data.CollectionViewSource)
                (this.FindResource("employeViewSource")));
            //employeViewSource.View.MoveCurrentToFirst();
            employeViewSource.View.MoveCurrentTo(null);

            // Chargez les données dans la table Projet. Vous pouvez modifier ce code si nécessaire.
            gestionProjetDataSetProjetTableAdapter = new Devoir2.GestionProjetDataSetTableAdapters.ProjetTableAdapter();
            gestionProjetDataSetProjetTableAdapter.Fill(gestionProjetDataSet.Projet);
            System.Windows.Data.CollectionViewSource projetViewSource = ((System.Windows.Data.CollectionViewSource)
                (this.FindResource("projetViewSource")));
            projetViewSource.View.MoveCurrentToFirst();
            // Chargez les données dans la table Affectation. Vous pouvez modifier ce code si nécessaire.
            gestionProjetDataSetAffectationTableAdapter = new Devoir2.GestionProjetDataSetTableAdapters.AffectationTableAdapter();
            gestionProjetDataSetAffectationTableAdapter.Fill(gestionProjetDataSet.Affectation);
            System.Windows.Data.CollectionViewSource affectationViewSource = ((System.Windows.Data.CollectionViewSource)
                (this.FindResource("affectationViewSource")));
            affectationViewSource.View.MoveCurrentToFirst();
            // Chargez les données dans la table projet_en_cours. Vous pouvez modifier ce code si nécessaire.
           gestionProjetDataSetprojet_en_coursTableAdapter = new Devoir2.GestionProjetDataSetTableAdapters.projet_en_coursTableAdapter();
            gestionProjetDataSetprojet_en_coursTableAdapter.Fill(gestionProjetDataSet.projet_en_cours);
            System.Windows.Data.CollectionViewSource projet_en_coursViewSource = ((System.Windows.Data.CollectionViewSource)
                (this.FindResource("projet_en_coursViewSource")));
            projet_en_coursViewSource.View.MoveCurrentToFirst();

            //a();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                Employe emp = new Employe
                {
                    nom = txtNom.Text.Trim(),
                    matricule = matriculeTextBox.Text.Trim()
                    ,
                    poste = posteTextBox.Text.Trim(),
                    salaire = int.Parse(salaireTextBox.Text.Trim()),
                    tel = txtTel.Text
                };

                db.Employe.InsertOnSubmit(emp);
                db.SubmitChanges();
                MessageBox.Show("Employe ajoute");
                gestionProjetDataSetEmployeTableAdapter.Fill(gestionProjetDataSet.Employe);

                a();
            }
            catch (Exception ex)
            {
               MessageBox.Show("Erreur d'insertion : " + ex.Message); 
            }
        }

        private void btnSaveProj_Click(object sender, RoutedEventArgs e)
        {
            try 
	        {	        
		        Projet p = new Projet { debut = DateTime.Parse(debutDatePicker.Text), 
                    fin = DateTime.Parse(finDatePicker.Text), nom = nomTextBox.Text };
	            db.Projet.InsertOnSubmit(p);
                db.SubmitChanges();
                MessageBox.Show("Projet bien ajouté");
                gestionProjetDataSetProjetTableAdapter.Fill(gestionProjetDataSet.Projet);
                gestionProjetDataSetprojet_en_coursTableAdapter.Fill(gestionProjetDataSet.projet_en_cours);
            }
	            catch (Exception ex)
	            {
		            MessageBox.Show("Erreur d'insertion : " + ex.Message); 
	            }

        }

        private void cbxProjet_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void idEmployeTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void btnSaveAff_Click(object sender, RoutedEventArgs e)
        {
            var empl = (from en in db.Employe where en.matricule.ToString().Equals(idEmployeTextBox.Text.ToString().Trim()) select en).FirstOrDefault();

            if (empl == null)
            {
                MessageBox.Show("L'employe n'existe pas");
                return;
            }
            
            try 
	            {	        
		            Affectation a = new Affectation { 
                        idEmploye = empl.idEmp,
                       idProjet = ((Projet)cbxProjet.SelectedItem).idProjet,
                            nbJours = int.Parse(nbJoursTextBox.Text.Trim())};
                        db.Affectation.InsertOnSubmit(a);
                        db.SubmitChanges();
                        MessageBox.Show("Affectation réussie!");
                        gestionProjetDataSetAffectationTableAdapter.Fill(gestionProjetDataSet.Affectation);
                        gestionProjetDataSetprojet_en_coursTableAdapter.Fill(gestionProjetDataSet.projet_en_cours);
	            }
	                catch (Exception ex)
	                {
		                MessageBox.Show("Erreur d'insertion : " + ex.Message); 
	                }
        }

        private void txtProj_KeyDown(object sender, KeyEventArgs e)
        {
            var list = from af in db.Affectation
                       where af.Projet.nom.Contains(txtProj.Text.Trim())
                       select
                           new {Nom = af.Employe.nom , 
                               Telephone = af.Employe.tel,
                                Salaire = af.Employe.salaire,
                                Poste = af.Employe.poste
                           };
            dtGrid.ItemsSource = list;
        }

        private void dtGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void employeListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMiz_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(
                    gestionProjetDataSetEmployeTableAdapter.Adapter);

                gestionProjetDataSetEmployeTableAdapter.Adapter.UpdateCommand = commandBuilder.GetUpdateCommand();
                //gestionProjetDataSetEmployeTableAdapter.Adapter.InsertCommand = commandBuilder.GetInsertCommand();
                gestionProjetDataSetEmployeTableAdapter.Adapter.Update(gestionProjetDataSet);
                gestionProjetDataSetEmployeTableAdapter.Adapter.Update(gestionProjetDataSet.Employe);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void idEmployeTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void a()
        {
            employeViewSource.View.MoveCurrentTo(null);

            matriculeTextBox.Text = "";
            nomTextBox.Text = "";
            matriculeTextBox.Text = "";
            idEmployeTextBox.Text = "";
            nbJoursTextBox.Text = "";
            txtNom.Text = "";
            txtNom.Text = "";
            salaireTextBox.Text = "";
            posteTextBox.Text = "";
            txtTel.Text = "";
           
        }

        private void btnVider_Click(object sender, RoutedEventArgs e)
        {
            a();
        }
    }
}
