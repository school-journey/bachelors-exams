﻿using Examen1.GraphikOnes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Examen1
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            new Model().ShowDialog();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
           
        }

        private void btnClass_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            new ClassSearch().ShowDialog();
            this.Close();
        }

        private void btnEtudiant_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            new StudentSearch().ShowDialog();
            this.Close();
        }
    }
}
