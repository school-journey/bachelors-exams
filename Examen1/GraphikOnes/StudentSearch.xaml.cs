﻿using Examen1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Examen1.GraphikOnes
{
    /// <summary>
    /// Logique d'interaction pour StudentSearch.xaml
    /// </summary>
    public partial class StudentSearch : Window
    {
        private LinqDataContext databaz = Connexion.Databaz;

        public StudentSearch()
        {
            InitializeComponent();

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {


            var listclasse = from i in databaz.Classe select i;
            cbxClasse.ItemsSource = listclasse;

            List<int> annee = new List<int>();
            for (int i = 1980; i <= DateTime.Now.Year; i++)
            {
                annee.Add(i);
            }

            cbxAnnee.ItemsSource = annee;
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            string clas = cbxAnnee.Text.Trim(), textan = cbxAnnee.Text.Trim(); 
            int annee = (int.TryParse(textan, out annee)) ? annee : 0;
            Classe classSelectionne = (Classe)cbxClasse.SelectedItem;

            if(clas.Length != 0 && annee != 0)
            {
                var list = from inscr in databaz.Inscription                           
                           where classSelectionne.idClasse == inscr.Classe.idClasse && annee == inscr.anneAcademique
                           select new { Annee_academique = inscr.anneAcademique, Classe = inscr.Classe.nomClasse, Matricule = inscr.Etudiant.matricule, Nom_complet = inscr.Etudiant.nomComplet, Tel = inscr.Etudiant.tel, Email = inscr.Etudiant.email, Naissance = inscr.Etudiant.dateNaiss };
                if(list.Count() == 0)
                {
                    aucunResulta.Visibility = Visibility.Visible;
                    dtgridEtudiant.Visibility = Visibility.Hidden;
                }else
                    {
                        aucunResulta.Visibility = Visibility.Hidden;
                        dtgridEtudiant.Visibility = Visibility.Visible;
                        dtgridEtudiant.ItemsSource = list;
                    }
            }else
                {
                    MessageBox.Show("Choisissez un élément dans chaque liste", "Champs vides", MessageBoxButton.OK, MessageBoxImage.Error);
                }


        }

        private void btnRetour_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            new MainWindow().ShowDialog();
            this.Close();
        }

        
    }
}
