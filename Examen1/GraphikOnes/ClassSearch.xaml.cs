﻿using Examen1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Examen1.GraphikOnes
{
    /// <summary>
    /// Logique d'interaction pour ClassSearch.xaml
    /// </summary>
    public partial class ClassSearch : Window
    {
        private LinqDataContext databaz = Connexion.Databaz;

        public ClassSearch()
        {
            InitializeComponent();
        }

        private void btnRechercher_Click(object sender, RoutedEventArgs e)
        {
            string matric = tbxMatricule.Text.Trim();

            if(matric.Length == 0)
            {
                MessageBox.Show("Renseignez le champ matricule", "CHAMP VIDE", MessageBoxButton.OK, MessageBoxImage.Error);
            }else
                {
                    var list = from clas in databaz.Classe
                               join insc in databaz.Inscription on clas.idClasse equals insc.idEEtudiantInscript
                               join etud in databaz.Etudiant on insc.idEEtudiantInscript equals etud.idEtudiant
                               where etud.matricule == matric
                               select new { Nom_classe = clas.nomClasse, Annee_academique = insc.anneAcademique, Matricule = etud.matricule };
                   
                    if(list.Count() == 0)
                    {
                        aucunTrouve.Visibility = Visibility.Visible;
                        dtgridAnneFreq.Visibility = Visibility.Hidden;
                    }else
                        {
                            dtgridAnneFreq.Visibility = Visibility.Visible;
                            dtgridAnneFreq.ItemsSource = list;
                            aucunTrouve.Visibility = Visibility.Hidden;
                        }
                    

                }
        }

        private void btnRetour_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            new MainWindow().ShowDialog();
            this.Close();
        }
    }
}
