﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen1.Models
{
    class Connexion
    {
        private static LinqDataContext Dbaz;
        
        public static LinqDataContext Databaz
        {
            get
                {
                    if (Dbaz == null)
                        Dbaz = new LinqDataContext();
                    return Dbaz;            
                }
        }
    }
}
