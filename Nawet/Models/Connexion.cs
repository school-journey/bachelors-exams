﻿using Nawet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nawet.Models
{
    class Connexion
    {
        private static ContextDbaz _baz;

        public static ContextDbaz baz
        {
            get
            {
                if (_baz == null)
                    _baz = new ContextDbaz();

                return _baz;
            }
        }
    }
}
