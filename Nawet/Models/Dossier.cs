﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nawet.Models
{
    public class Dossier
    {
        [Key]
        public int id { get; set; }

        public Dossier()
        {

        }

        public DateTime dateOuverture
        {
            get;
            set;
        }

        public DateTime dateCloture
        {
            get;
            set;
        }

        public string numDossier
        {
            get;
            set;
        }

        public string etat
        {
            get;
            set;
        }

        public string nomExpert
        {
            get;
            set;
        }

        public Contrat contrat
        {
            get;
            set;
        }

    }
}
