﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nawet.Models
{
    public class Contrat
    {
        [Key]
        public int id { get; set; }

        
        public DateTime date
        {
            get;
            set;
        }

        public string clause
        {
            get;
            set;
        }

        public string matricule
        {
            get;
            set;
        }

        public string marque
        {
            get;
            set;
        }

        public string puissance_fiscale
        {
            get;
            set;
        }

        public Assure assure
        {
            get;
            set;
        }

        public float poids
        {
            get;
            set;
        }

    }
}
