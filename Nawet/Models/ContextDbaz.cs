﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Nawet.Models
{
    public class ContextDbaz : DbContext
    {
        public ContextDbaz()
            : base(@"Data Source=KINGWIN8\SQLEXPRESS;Initial Catalog=CSharpNawet;Integrated Security=True")
        {

        }

        public DbSet<Contrat> contrat { get; set; }
        public DbSet<Dossier> dossier { get; set; }
        public DbSet<Assure> assure { get; set; }
    }
}
