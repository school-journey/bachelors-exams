﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nawet.Models
{
    public class Assure
    {        
        [Key]
        public int id { get; set; }

        [Index("unikCode", Order = 1, IsUnique = true)]     
        [StringLength(45)]
        [Required]
        public string code
        {
            get;
            set;
        }

        [StringLength(45)]
        [Required]
        public string nom
        {
            get;
            set;
        }

        [Index("unikTel", Order = 1, IsUnique = true)]
        [StringLength(45)]
        [Required]
        public string tel
        {
            get;
            set;
        }
    }
}
