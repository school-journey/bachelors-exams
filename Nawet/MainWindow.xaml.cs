﻿using Nawet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Nawet
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ContextDbaz baz = Connexion.baz;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Ecn cours
            var listcour = (from dos in baz.dossier
                            where dos.etat.StartsWith("en")
                            select
                                new { Numero_dossier = dos.numDossier, Date_ouverture  = dos.dateOuverture, Expert = dos.nomExpert,
                                Matricule_contrat = dos.contrat.matricule, Nom_Assure = dos.contrat.assure.nom, 
                                Tel = dos.contrat.assure.tel, Etat = dos.etat
                                }
                                ).ToList();
            dtGridCours.ItemsSource = listcour;

            //-----------------

            var listcloz = (from dos in baz.dossier
                            where dos.etat.StartsWith("clo")
                            select
                                new
                                {                                    
                                    Matricule_contrat = dos.contrat.matricule,
                                    Puissance_fiscale = dos.contrat.puissance_fiscale,
                                    Numero_dossier = dos.numDossier,
                                    Date_ouverture = dos.dateOuverture,
                                    Date_fermetture = dos.dateCloture
                                    
                                }
                    ).ToList();

            dtGridCours1.ItemsSource = listcloz;
        }

        private void btnCher_Click(object sender, RoutedEventArgs e)
        {
            string cod = txtcoDESearch.Text.Trim();

            if(cod.Length > 0)
            {

                var listclo = (from dos in baz.dossier
                                where dos.etat.StartsWith("clo") && dos.contrat.assure.code.Equals(cod)
                                select
                                    new
                                    {
                                        Matricule_contrat = dos.contrat.matricule,
                                        Puissance_fiscale = dos.contrat.puissance_fiscale,
                                        Numero_dossier = dos.numDossier,
                                        Date_ouverture = dos.dateOuverture,
                                        Date_fermetture = dos.dateCloture

                                    }
                        ).ToList();

                dtGridCours1.ItemsSource = listclo;
            }
            else
            {
                var listclo = (from dos in baz.dossier
                               where dos.etat.StartsWith("clo") 
                               select
                                   new
                                   {
                                       Matricule_contrat = dos.contrat.matricule,
                                       Puissance_fiscale = dos.contrat.puissance_fiscale,
                                       Numero_dossier = dos.numDossier,
                                       Date_ouverture = dos.dateOuverture,
                                       Date_fermetture = dos.dateCloture

                                   }
                        ).ToList();

                dtGridCours1.ItemsSource = listclo;

            }
        }
    }
}
